import { createApp } from 'vue'
import './assets/css/style.css'
import App from './App.vue'
import { router } from './router/router.js'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/reset.css'
import './assets/css/antd.css'
import { createPinia } from 'pinia'

const app = createApp(App)

app.use(router)
app.use(Antd)
app.use(createPinia())
app.mount('#app')
