import { ofetch } from 'ofetch'

const apiUat = ofetch.create({
  baseURL: `${import.meta.env.VITE_API_UAT_ROOT}/api`,
  headers: {
    accept: 'application/ld+json',
    'Content-Type': 'application/ld+json'
  },
  onRequest({ options }) {
    const token = localStorage.getItem('access_token')

    if (token) {
      options.headers.Authorization = `Bearer ${token}`
    }
  }
})

export { apiUat }
