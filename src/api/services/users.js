export const users = (api) => ({
  register: (data) => api('/users/register', { method: 'POST', body: data }),
  getCurrentUser: () => api('/users/me'),
  resendVerificationEmail: (userEmail, userId) =>
    api(`/users/${userId}/resend_verification_email`, {
      query: { email: userEmail }
    })
})
