import { defineStore } from 'pinia'

export const useMainStore = defineStore({
  id: 'main-store',

  state: () => ({
    user: null
  })
})
