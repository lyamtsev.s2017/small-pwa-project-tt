export const routeNames = {
  login: 'login',
  register: 'register',
  dashboard: 'dashboard',
  validateEmail: 'validate-email'
}

export const routes = [
  {
    path: '/login',
    name: routeNames.login,
    component: () => import('../views/LoginPage/LoginPage.vue'),
    meta: {
      isAuthOptional: true
    }
  },
  {
    path: '/register',
    name: routeNames.register,
    component: () => import('../views/RegisterPage/RegisterPage.vue'),
    meta: {
      isAuthOptional: true
    }
  },
  {
    path: '/dashboard',
    alias: '/',
    name: routeNames.dashboard,
    component: () => import('../views/UserDashboard/UserDashboard.vue')
  },
  {
    path: '/validate-email/:userId/:token',
    name: routeNames.validateEmail,
    component: () => import('../views/ValidateEmail/ValidateEmail.vue'),
    meta: {
      isAuthOptional: true
    }
  }
]
